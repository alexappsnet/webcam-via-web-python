# README #

### What is this repository for? ###

This is a short sample showing how to use Tornado+JQuery to present a "video stream" from home WebCam to client Web Browser.

Check out the related article on http://alexapps.net/web-browser-show-remote-webcam/.

### How do I get set up? ###

* Have: Windows or Linux or Mac OS with WebCam
* Install: Python 2.7, OpenCV, Tornado
* Run: "> python run.py" and in Web Browser type "http://localhost:8001"

### Tested on ###

* **Ubuntu 14** + Python 2.7 + NumPy 1.8.2 + OpenCV 2.4.10 + Tornado 4.0.2 - **SUCCESS**
* **Windows 7** + Python 2.7 + OpecCV 2.4.10 + Tornado 4.0.2 - **SUCCESS**
* **Mac OS X 10** + Python 2.7 + OpecCV 2.4.10 + Tornado 4.1 - **SUCCESS**